Object.prototype.size = function() {return Object.keys(this).length};
var weekDays = ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
    months = ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"];
//123
$(function(){

    var data, dl,
        currentFrom,
        currentTo,
        currentPeriod,
        isOffset,
        isBegin = false;

    var $getAll = $('.diary__header-all'),
        $getCurrent = $('.diary__header-current'),
        $wrap = $('.diary__week'),
        $current = $('.header__dates-arrow-current'),
        $goNext = $('.header__dates-arrow_next'),
        $goPrev = $('.header__dates-arrow_prev'),
        $getPeriod = $('.diary__header-show-period'),
        $showPeriod = $('.diary__header-showperiod'),
        $periodPop = $('.diary__header-period'),
        $periodError = $('.diary__header-perioderror');

    var oneDay = 1000 * 60 * 60 * 24,
        now = new Date(),
        firstYear,
        secondYear;

    if(now.getMonth() > 7) {
        firstYear = parseInt(now.getFullYear());
        secondYear = firstYear + 1;
    } else {
        secondYear = parseInt(now.getFullYear());
        firstYear = secondYear - 1;
    }

    var start = new Date(firstYear, 8, 1),
        diff = now - start,
        todayDay = Math.floor(diff / oneDay);

        currentFrom = todayDay; // Текущий номер дня в учебном году начиная с 1
        currentTo = currentFrom + 6;

    console.log(currentFrom);
    console.log(currentTo);
    console.log(oneDay);

    function getScedual(user){
        $.ajax({
            url: 'test.json'
        }).done(function (response) {
            data = response.data;
            dl = data.length - 1;

            $('.diary__header-name').text(response.username);

            console.log('current day num: ' + currentFrom);

            drawScedual(true);
        });
    }

    function drawScedual(isInit){

        $wrap.empty();
        isOffset = false;

        var targetMilliseconds = (start - 0) + currentFrom*oneDay,
            target = new Date(targetMilliseconds),
            dayCounter = target.getDay();


        if(isInit){
            var dayOffset = 0;
            if(dayCounter == 0) dayOffset = -1;
            if(dayCounter > 1) dayOffset = dayCounter - 1;

            dayCounter = 1;
            isOffset = true;

            currentFrom-=dayOffset;
            currentTo-=dayOffset;

            console.log('Start init from ' + weekDays[dayCounter]);
            isOffset = false;
        }

        if(currentFrom < 0) {
            dayCounter-=currentFrom;
            currentFrom = 0;
            $goPrev.hide();
            isBegin = true;
        } else {
            $goPrev.show();
        }

        if(currentFrom > dl){
            currentFrom = dl;
        }

        if(currentTo > dl) {
            currentTo = dl;
            $goNext.hide();
        } else {
            $goNext.show();
        }

        var dateFrom = new Date(start - 0 + currentFrom*oneDay),
            dateTo = new Date(start - 0 + currentTo*oneDay),
            resDate = (currentFrom == currentTo) ? dateFrom.getDate() + ' ' + months[dateFrom.getMonth()] : dateFrom.getDate() + ' ' + months[dateFrom.getMonth()] + ' - ' + dateTo.getDate() + ' ' + months[dateTo.getMonth()],
            yearFrom = dateFrom.getFullYear(),
            yearTo = dateTo.getFullYear(),
            resYear = (yearFrom == yearTo) ? yearFrom : yearFrom + ' - ' + yearTo;


        $current.html(resDate + ', <b>' + resYear + '</b>');

        for(var a = currentFrom; a < currentTo + 1; a++){

            var todayMS = start - 0 + a*oneDay,
                today = new Date(todayMS);


            if(data.hasOwnProperty(a)){

                var isSunday = (dayCounter == 0) ? ' diary__day_sunday' : '';
                var isToday = (todayDay == a) ? ' diary__day_today' : '';

                var $template = $('<div/>')
                    .addClass('diary__day' + isSunday + isToday)
                    .attr('data-day', a)
                    .append(
                    $('<div/>')
                        .addClass('diary__day-name')
                        .append($('<span/>').text(weekDays[dayCounter] + ', ' + today.getDate() + ' ' + months[today.getMonth()])))
                    .append($('<div/>')
                        .addClass('diary__day-content')
                        .append(function(){

                            var lessons = '';

                            if(data[a].size() > 0){

                                for(var b in data[a]){
                                    if(data[a].hasOwnProperty(b)){

                                        var discipline = data[a][b].discipline || '<i>Окно</i>',
                                            notes = data[a][b].notes || '',
                                            mark = data[a][b].mark || '',
                                            percent = data[a][b].percent || '',
                                            time = (b-0) + 10;

                                        lessons += '<div class="diary__lesson" data-position="' + b + '">' +
                                            '<div class="diary__lesson-cell diary__lesson-cell_number">' + (b - 0 + 1) + '</div>' +
                                            '<div class="diary__lesson-cell diary__lesson-cell_time">' + time + ':00'  + '</div>' +
                                            '<div class="diary__lesson-cell diary__lesson-cell_discipline">' + discipline + '</div>' +
                                            '<div class="diary__lesson-cell diary__lesson-cell_notes">' + notes + '</div>' +
                                            '<div class="diary__lesson-cell diary__lesson-cell_mark"><div class="mark__edit"><div class="edit__deny"></div><input class="edit__input" type="text" data-mark="' + mark + '" value="' + mark + '"><div class="edit__accept"></div><div class="edit__change"></div></div></div>' +
                                            '<div class="diary__lesson-cell diary__lesson-cell_mark_practical">' + percent + '</div>' +
                                            '</div>';

                                    }

                                }

                            } else {

                                lessons = $('<div/>', { class: "diary__weekend" } );

                            }

                            return lessons;

                        }));

                $wrap.append($template);

                if(dayCounter < 6) dayCounter++;
                else {
                    dayCounter = 0;
                }

            }

        }

        $('body, html').scrollTop(0);


        $('.mark__edit').each(function(){
            var $wrap = $(this),
                $mark = $wrap.find('.edit__input'),
                $edit = $wrap.find('.edit__change'),
                $accept = $wrap.find('.edit__accept'),
                $deny = $wrap.find('.edit__deny'),
                val = false,
                prevVal = null;

            $edit.click(function(){
                $mark.focus();
            });

            $mark.focus(function(){
                $wrap.addClass('mark__edit_active');
                prevVal = $mark.val();
            });

            $mark.keyup(function(){
                val = $mark.val();
            });

            $deny.click(function(){
                $mark.val(prevVal);
                $wrap.removeClass('mark__edit_active');

                console.log('prev ' + prevVal);
                console.log('cur ' + val);
            });

            $accept.click(function(){
                if(val) $mark.val(val);

                $wrap.removeClass('mark__edit_active');
            });

            $mark.blur(function(){
                setTimeout(function(){
                    $wrap.removeClass('mark__edit_active');
                }, 200)
            });
        });
    }

    function skipPeriod(x){

        if(data){

            currentPeriod = currentTo - currentFrom + 1;

            currentFrom = currentFrom + x*currentPeriod;
            currentTo = currentTo + x*currentPeriod;

            if(isBegin) {
                currentTo++; // костыль
                isBegin = false;
            }

            drawScedual(isOffset);

        }
    }

    function periodError(txt){
        $periodError.text(txt).show();
        setTimeout(function(){
            $periodError.text('').hide();
        }, 1000);
    }

    $getAll.click(function(){
        currentFrom = 0;
        currentTo = data.length - 1;

        drawScedual();

        $getAll.hide();
        $getCurrent.show();
        $goNext.hide();
        $goPrev.hide();
    });

    $getCurrent.click(function(){
        currentFrom = Math.floor(diff / oneDay);
        currentTo = currentFrom + 6;

        drawScedual(true);

        $getAll.show();
        $getCurrent.hide();
        $goNext.show();
        $goPrev.show();
    });

    $goNext.click(function(){skipPeriod(1)});
    $goPrev.click(function(){skipPeriod(-1)});

    $getPeriod.click(function(){
        $periodPop.toggle();
    });

    $showPeriod.click(function(){

        var day = 1000*60*60*24,
            sf = Math.floor((Date.parse($('#scFrom').val()) - (new Date( (new Date).getFullYear(), 8, 1) ))/day),
            st = Math.floor((Date.parse($('#scTo').val()) - (new Date( (new Date).getFullYear(), 8, 1) ))/day);

        if(sf >= 0 && sf < dl && st >= 0 && st < dl){

            if(st > sf){

                currentFrom = sf;
                currentTo = st;

                drawScedual();

                $goNext.hide();
                $goPrev.hide();
                $getAll.hide();
                $getCurrent.show();

                $periodPop.hide();

            } else {

                periodError('Ошибка: неверный порядок');

            }

        } else {

            if(st < 0 || st > dl || sf < 0 || sf > dl){

                periodError('Ошибка: даты вне учебного года');

            } else {

                periodError('Ошибка: не указаны даты');

            }
        }

    });

    $(document).ready(getScedual);

    // Debug

    $('.diary__header-title').click(function(){
        console.log('from ' + currentFrom);
        console.log('to ' + currentTo);
        console.log('data ' + data.length);
    });

});
